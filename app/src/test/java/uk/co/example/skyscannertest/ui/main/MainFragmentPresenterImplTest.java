package uk.co.example.skyscannertest.ui.main;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

/**
 * Simple Test for the MainFragmentPresenter
 */
@RunWith(MockitoJUnitRunner.class)
public class MainFragmentPresenterImplTest {

    @Mock
    MainFragmentView mainFragmentView;
    MainFragmentPresenterImpl mainFragmentPresenter;

    @Before
    public void setUp() {
        mainFragmentPresenter = new MainFragmentPresenterImpl();
        mainFragmentPresenter.attach(mainFragmentView);
    }

    @Test
    public void whenStartSearchButtonPressed_launchResultFramentIsInvoked() throws Exception {
        final Date outgoingDate = new Date();
        final Date ingoingDate = new Date();
        mainFragmentPresenter.startSearchButtonPressed(outgoingDate, ingoingDate);

        Mockito.verify(mainFragmentView).launchResultFragment(outgoingDate, ingoingDate);
    }

}