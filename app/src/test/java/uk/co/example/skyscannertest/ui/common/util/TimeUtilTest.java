package uk.co.example.skyscannertest.ui.common.util;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

/**
 * Unit Test for the TimeUtil class
 */
public class TimeUtilTest {

    private static final int TEST_DAY = 28;
    private static final int TEST_MONTH = 6;
    private static final int TEST_YEAR = 1970;

    private Date mRefDate;
    private Calendar mStartDate;

    @Before
    public void setUp() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(TEST_YEAR, TEST_MONTH, TEST_DAY);
        mRefDate = calendar.getTime();
        mStartDate = Calendar.getInstance();
        mStartDate.setTime(mRefDate);
    }

    @Test
    public void durationToStringLessThan60ReturnsTheSame() throws Exception {
        final int duration = 45;
        final String durationString = TimeUtil.durationToString(duration);
        Assert.assertEquals("45m", durationString);
    }

    @Test
    public void durationToStringMoreThan60ReturnsTheHours() throws Exception {
        final int duration = 145;
        final String durationString = TimeUtil.durationToString(duration);
        Assert.assertEquals("2h 25m", durationString);
    }

    @Test
    public void durationToStringExactly60ReturnsOneHour() throws Exception {
        final int duration = 60;
        final String durationString = TimeUtil.durationToString(duration);
        Assert.assertEquals("1h 0m", durationString);
    }

    @Test
    public void nextDayOf() throws Exception {
        Assert.assertEquals(Calendar.TUESDAY, mStartDate.get(Calendar.DAY_OF_WEEK));
        final Date nextMonday = TimeUtil.nextDayOf(mStartDate.getTime(), Calendar.MONDAY);
        final Calendar result = Calendar.getInstance();
        result.setTime(nextMonday);
        Assert.assertEquals(Calendar.MONDAY, result.get(Calendar.DAY_OF_WEEK));
        Assert.assertTrue(result.after(mStartDate));
    }

    @Test
    public void tomorrow() throws Exception {
        final Date tomorrow = TimeUtil.tomorrow(mRefDate);
        final Calendar result = Calendar.getInstance();
        result.setTime(mRefDate);
        result.roll(Calendar.DATE, 1);
        Assert.assertEquals(tomorrow, result.getTime());

    }

    @Test
    public void asString() throws Exception {
        final String asString = TimeUtil.asString(mRefDate);
        Assert.assertEquals("1970-07-28", asString);
    }

    @Test
    public void asShortString() throws Exception {
        final String asString = TimeUtil.asShortString(mRefDate);
        Assert.assertEquals("28 Jul", asString);
    }

}