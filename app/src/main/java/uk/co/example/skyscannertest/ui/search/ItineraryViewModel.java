package uk.co.example.skyscannertest.ui.search;

import java.util.LinkedList;
import java.util.List;

import uk.co.example.skyscannertest.ui.view.AgentView;

/**
 * This is the ViewModel for each Itinerary
 */

public class ItineraryViewModel {

    private AgentViewModel mAgentViewModel;

    private List<SegmentViewModel> mSegments = new LinkedList<>();

    public void addSegment(SegmentViewModel segmentViewModel) {
        mSegments.add(segmentViewModel);
    }

    public List<SegmentViewModel> getSegments() {
        return mSegments;
    }


    public void setAgentViewModel(AgentViewModel agentViewModel) {
        this.mAgentViewModel = agentViewModel;
    }

    public AgentViewModel getAgentViewModel() {
        return mAgentViewModel;
    }
}
