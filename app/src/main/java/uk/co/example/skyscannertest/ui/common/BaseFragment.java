package uk.co.example.skyscannertest.ui.common;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import uk.co.example.skyscannertest.SkyScanApplication;
import uk.co.example.skyscannertest.dagger.SkyScanComponent;
import uk.co.example.skyscannertest.ui.NavigationService;
import uk.co.example.skyscannertest.ui.common.mvp.Presenter;
import uk.co.example.skyscannertest.ui.common.mvp.View;

/**
 * This is the base fragment which manages interaction with Presenter
 */

public abstract class BaseFragment<P extends Presenter<V>, V extends View> extends Fragment implements View {

    @Inject
    protected P mPresenter;

    private NavigationService mNavigationService;

    private ActionBar mActionBar;

    @Override
    public void onViewCreated(@NonNull android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.attach(getFragmentView());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NavigationService) {
            mNavigationService = (NavigationService) context;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getContext() instanceof AppCompatActivity) {
            final AppCompatActivity appCompatActivity = (AppCompatActivity) getContext();
            mActionBar = appCompatActivity.getSupportActionBar();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        mPresenter.detach();
        mPresenter = null;
        super.onDestroy();
    }

    /**
     * Specific Fragments have to define who is the View
     */
    protected V getFragmentView() {
        return (V) this;
    }

    /**
     * @return The SkyScannerComponent for injection
     */
    protected SkyScanComponent getComp() {
        return ((SkyScanApplication) getActivity().getApplication()).getSkyScanComponent();
    }

    /**
     * @return The NavigationService if provided
     */
    protected NavigationService getNavigationService() {
        return mNavigationService;
    }

    /**
     * @return The ActionBar
     */
    protected ActionBar getActionBar() {
        return mActionBar;
    }
}
