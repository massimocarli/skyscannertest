package uk.co.example.skyscannertest.ui.common.mvp;

/**
 * Presenter abstraction in the MVP architectural pattern
 */

public interface Presenter<View> {

    /**
     * Invoked during the specific View lifecycle in order to get pass the presenter the
     * related View
     *
     * @param view The View used from the presenter
     */
    void attach(View view);

    /**
     * Invoked during the View lifecycle in order to release resources
     */
    void detach();
}
