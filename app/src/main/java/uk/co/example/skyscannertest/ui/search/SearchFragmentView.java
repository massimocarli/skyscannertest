package uk.co.example.skyscannertest.ui.search;

import java.util.List;

import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.ui.common.mvp.View;

/**
 * This is the View for the Main fragment
 */

public interface SearchFragmentView extends View {

    void updateItineraryList(List<ItineraryViewModel> itineraryViewModels);

    void updateSearchTitle(ActionBarViewModel searchTitleModel);

    void displayError(Throwable throwable);

    void displayProgressDialog();

    void hideProgressDialog();

    void searchCompleted();
}
