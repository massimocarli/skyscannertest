package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model for Carrier
 */

public class Carrier {

    @SerializedName("Id")
    private int mId;

    @SerializedName("Code")
    private String mCode;

    @SerializedName("Name")
    private String mName;

    @SerializedName("ImageUrl")
    private String mImageUrl;

    @SerializedName("DisplayCode")
    private String mDisplayCode;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getDisplayCode() {
        return mDisplayCode;
    }

    public void setDisplayCode(String mDisplayCode) {
        this.mDisplayCode = mDisplayCode;
    }
}
