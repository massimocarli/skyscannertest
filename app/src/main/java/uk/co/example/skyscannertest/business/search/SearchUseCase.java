package uk.co.example.skyscannertest.business.search;

import io.reactivex.Observable;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.SearchRequest;
import uk.co.example.skyscannertest.ui.common.mvp.UseCase;

/**
 * The UseCase for flight Search
 */

public interface SearchUseCase extends UseCase {

    /**
     * Coordinate the usage of API in order to get the information about a search for a flight
     *
     * @param searchRequest Encapsulate the input parameter for the request
     * @return An Observable with the PollingResult
     */
    Observable<PollingResult> searchFlight(SearchRequest searchRequest);
}
