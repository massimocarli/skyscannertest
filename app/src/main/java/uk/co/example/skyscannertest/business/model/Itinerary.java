package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model class for Itinerary
 */

public class Itinerary {

    public static class PricingOption {

        @SerializedName("Agents")
        public int[] mAgents;

        @SerializedName("QuoteAgeInMinutes")
        public int mQuoteAgeInMinutes;

        @SerializedName("Price")
        public float mPrice;

        @SerializedName("DeeplinkUrl")
        public String mDeeplinkUrl;
    }

    public static class BookingDetailsLink {

        @SerializedName("Uri")
        private String mUri;

        @SerializedName("Body")
        private String mBody;

        @SerializedName("Method")
        private String mMethod;
    }

    @SerializedName("OutboundLegId")
    private String mOutboundLegId;

    @SerializedName("InboundLegId")
    private String mInboundLegId;

    @SerializedName("PricingOptions")
    private PricingOption[] mPricingOptions;

    @SerializedName("BookingDetailsLink")
    private BookingDetailsLink mBookingDetailsLink;

    public String getOutboundLegId() {
        return mOutboundLegId;
    }

    public String getInboundLegId() {
        return mInboundLegId;
    }

    public PricingOption[] getPricingOptions() {
        return mPricingOptions;
    }

    public BookingDetailsLink getBookingDetailsLink() {
        return mBookingDetailsLink;
    }
}
