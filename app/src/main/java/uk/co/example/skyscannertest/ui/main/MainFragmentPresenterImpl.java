package uk.co.example.skyscannertest.ui.main;

import java.util.Date;

import uk.co.example.skyscannertest.ui.common.BasePresenter;

/**
 * This is the implementation for the MainFragmentPresenter
 */

public class MainFragmentPresenterImpl extends BasePresenter<MainFragmentView> implements MainFragmentPresenter {

    @Override
    public void startSearchButtonPressed(Date outboundDate, Date inboundDate) {
        getView().launchResultFragment(outboundDate, inboundDate);
    }
}
