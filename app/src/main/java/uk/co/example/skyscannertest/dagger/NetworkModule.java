package uk.co.example.skyscannertest.dagger;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.example.skyscannertest.business.api.SkyScannerApi;

/**
 * This is the Module for Network management
 */
@Module
public class NetworkModule {

    private static final int THREAD_POOL_SIZE = 5;
    private static int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Context context) {
        Cache cache = new Cache(context.getCacheDir(), CACHE_SIZE);
        return new OkHttpClient.Builder()
                .cache(cache)
                .build();
    }

    @Provides
    @Singleton
    public SkyScannerApi provideSkyScannerApi(OkHttpClient okHttpClient) {
        final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://partners.api.skyscanner.net/apiservices/")
                .callbackExecutor(Executors.newFixedThreadPool(THREAD_POOL_SIZE))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(SkyScannerApi.class);
    }
}
