package uk.co.example.skyscannertest.ui.common;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import uk.co.example.skyscannertest.SkyScanApplication;
import uk.co.example.skyscannertest.dagger.SkyScanComponent;
import uk.co.example.skyscannertest.ui.NavigationService;
import uk.co.example.skyscannertest.ui.common.mvp.Presenter;
import uk.co.example.skyscannertest.ui.common.mvp.View;

/**
 * Base Activity with integration with Presenter lifecycle
 */

public abstract class BaseActivity<P extends Presenter<V>, V extends View> extends AppCompatActivity implements View, NavigationService {

    @Inject
    protected P mPresenter;

    @Override
    protected void onStart() {
        mPresenter.attach(getView());
        super.onStart();
    }

    @Override
    protected void onStop() {
        mPresenter.detach();
        mPresenter = null;
        super.onStop();
    }

    /**
     * Activities have to define who is the View
     */
    protected V getView() {
        return (V) this;
    }

    /**
     * @return The SkyScannerComponent for injection
     */
    protected SkyScanComponent getComp() {
        return ((SkyScanApplication) getApplication()).getSkyScanComponent();
    }

    @Override
    public void replaceFragmentAt(int anchorId, Fragment fragment) {
        replaceFragmentAt(anchorId, fragment, false);
    }

    @Override
    public void replaceFragmentAt(int anchorId, Fragment fragment, boolean addToBackStack) {
        final FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.replace(anchorId, fragment).commit();
    }

    @Override
    public void replaceFragmentAt(int anchorId, Fragment fragment, String backStack) {
        getSupportFragmentManager()
                .beginTransaction().addToBackStack(backStack)
                .replace(anchorId, fragment)
                .commit();
    }

    @Override
    public void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }
}
