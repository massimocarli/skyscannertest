package uk.co.example.skyscannertest.ui.search;

/**
 * This is the ViewModel for the ActionBar
 */

public class ActionBarViewModel {

    private static final String EMPTY_VAUE = "--";

    public final String flightDestination;
    public final String flightRequestInfo;


    private ActionBarViewModel(Builder builder) {
        flightDestination = builder.mFlightDestination;
        flightRequestInfo = builder.mFlightRequestInfo;
    }

    public static class Builder {

        private String mFlightDestination = EMPTY_VAUE;
        private String mFlightRequestInfo = EMPTY_VAUE;

        private Builder() {
        }

        public static final Builder create() {
            return new Builder();
        }

        public Builder withFlightDestination(final String flightDestination) {
            mFlightDestination = flightDestination;
            return this;
        }

        public Builder withFlightRequestInfo(final String flightRequestInfo) {
            mFlightRequestInfo = flightRequestInfo;
            return this;
        }

        public ActionBarViewModel build() {
            return new ActionBarViewModel(this);
        }
    }
}
