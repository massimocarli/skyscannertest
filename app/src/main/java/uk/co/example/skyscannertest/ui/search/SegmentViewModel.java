package uk.co.example.skyscannertest.ui.search;

/**
 * This is the ViewModel for the Segment of an Itinerary
 */

public class SegmentViewModel {

    private static final String EMPTY_VAUE = "--";

    public final String flightInfoTime;
    public final String flightInfoAirports;
    public final String mFlightDurationType;
    public final String mFlightDurationValue;
    public final String mCarrierIconUrl;

    private SegmentViewModel(Builder builder) {
        flightInfoTime = builder.mFlightInfoTime;
        flightInfoAirports = builder.mFlightInfoAirports;
        mFlightDurationType = builder.mFlightDurationType;
        mFlightDurationValue = builder.mFlightDurationValue;
        mCarrierIconUrl = builder.mCarrierIconUrl;
    }


    public static class Builder {

        private String mFlightInfoTime = EMPTY_VAUE;
        private String mFlightInfoAirports = EMPTY_VAUE;
        private String mFlightDurationType = EMPTY_VAUE;
        private String mFlightDurationValue = EMPTY_VAUE;
        private String mCarrierIconUrl;

        private Builder() {
        }

        public static final Builder create() {
            return new Builder();
        }

        public Builder withFlightInfoTime(final String flightInfoTime) {
            mFlightInfoTime = flightInfoTime;
            return this;
        }

        public Builder withFlightInfoAirports(final String flightInfoAirports) {
            mFlightInfoAirports = flightInfoAirports;
            return this;
        }

        public Builder withFlightDurationType(final String flightDurationType) {
            mFlightDurationType = flightDurationType;
            return this;
        }

        public Builder withFlightDurationValue(final String flightDurationValue) {
            mFlightDurationValue = flightDurationValue;
            return this;
        }

        public Builder withCarrierIconUrl(final String carrierIconUrl) {
            mCarrierIconUrl = carrierIconUrl;
            return this;
        }

        public SegmentViewModel build() {
            return new SegmentViewModel(this);
        }
    }

}
