package uk.co.example.skyscannertest.ui.main;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;
import java.util.Date;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.BaseFragment;
import uk.co.example.skyscannertest.ui.common.util.TimeUtil;
import uk.co.example.skyscannertest.ui.search.SearchResultFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends BaseFragment<MainFragmentPresenter, MainFragmentView> implements MainFragmentView {


    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onAttach(Context context) {
        getComp().inject(this);
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.launchSearchButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // We get the date of the first
                final Date nextMonday = nextMonday();
                final Date dayAfter = TimeUtil.tomorrow(nextMonday);

                mPresenter.startSearchButtonPressed(nextMonday, dayAfter);
            }
        });
    }

    @Override
    public void launchResultFragment(Date outboundDate, Date inboundDate) {
        getNavigationService().replaceFragmentAt(R.id.anchor_point,
                SearchResultFragment.newInstance(outboundDate, inboundDate), true);
    }

    private Date nextMonday() {
        return TimeUtil.nextDayOf(Calendar.getInstance().getTime(), Calendar.MONDAY);
    }
}
