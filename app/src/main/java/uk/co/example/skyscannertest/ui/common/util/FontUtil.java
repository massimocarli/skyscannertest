package uk.co.example.skyscannertest.ui.common.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Utility class for Font loading
 */

public class FontUtil {

    private Typeface mRobotoMediumTypeface;
    private Typeface mRobotoRegularTypeface;

    private static FontUtil sInstance;

    private FontUtil(Context context) {
        mRobotoMediumTypeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Medium.ttf");
        mRobotoRegularTypeface = Typeface.createFromAsset(context.getAssets(), "font/Roboto-Regular.ttf");
    }

    public static final FontUtil get(Context context) {
        if (sInstance == null) {
            sInstance = new FontUtil(context);
        }
        return sInstance;
    }

    public Typeface getRobotoMediumTypeface() {
        return mRobotoMediumTypeface;
    }

    public Typeface getRobotoRegularTypeface() {
        return mRobotoRegularTypeface;
    }
}
