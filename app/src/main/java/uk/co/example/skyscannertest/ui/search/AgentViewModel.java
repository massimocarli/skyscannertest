package uk.co.example.skyscannertest.ui.search;

/**
 * This is the ViewModel for the Agent of an Itinerary
 */

public class AgentViewModel {

    private static final String EMPTY_VAUE = "--";
    private static final float DEFAULT_RATE_VALUE = 0.5f;

    public final String agentPrice;
    public final String agentName;
    public final float agentRateValue;


    private AgentViewModel(Builder builder) {
        agentPrice = builder.mAgentPrice;
        agentName = builder.mAgentName;
        agentRateValue = builder.mAgentRateValue;
    }


    public static class Builder {

        private String mAgentPrice = EMPTY_VAUE;
        private String mAgentName = EMPTY_VAUE;
        private float mAgentRateValue = DEFAULT_RATE_VALUE;


        private Builder() {
        }

        public static final Builder create() {
            return new Builder();
        }

        public Builder withAgentPrice(final String agentPrice) {
            mAgentPrice = agentPrice;
            return this;
        }

        public Builder withAgentName(final String agentName) {
            mAgentName = agentName;
            return this;
        }

        public Builder withAgentRateValue(final float agentRateValue) {
            mAgentRateValue = agentRateValue;
            return this;
        }


        public AgentViewModel build() {
            return new AgentViewModel(this);
        }
    }

}
