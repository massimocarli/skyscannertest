package uk.co.example.skyscannertest.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.FontUtil;
import uk.co.example.skyscannertest.ui.common.util.UI;

/**
 * This is a custom View that allows to display 2 different strings. This isa typical
 * compound custom view
 */

public class TwoLinesTextView extends LinearLayout {

    private static final int NO_TEXT_SIZE = -1;

    private TextView mUpperTextView;
    private TextView mLowerTextView;

    private int mLowerTextSize;
    private int mUpperTextSize;
    private int mLowerTextColor;
    private int mUpperTextColor;
    private int mTextAlignment = 0;

    public TwoLinesTextView(Context context) {
        super(context);
        init(context, null);
    }


    public TwoLinesTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TwoLinesTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        setOrientation(LinearLayout.VERTICAL);
        if (attrs != null) {

        }
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TwoLinesTextView,
                0, 0);
        try {
            mLowerTextSize = a.getDimensionPixelSize(R.styleable.TwoLinesTextView_lowerTextSize, NO_TEXT_SIZE);
            mLowerTextColor = a.getColor(R.styleable.TwoLinesTextView_lowerTextColor, mLowerTextColor);
            mUpperTextSize = a.getDimensionPixelSize(R.styleable.TwoLinesTextView_upperTextSize, NO_TEXT_SIZE);
            mUpperTextColor = a.getColor(R.styleable.TwoLinesTextView_upperTextColor, mUpperTextColor);
            mTextAlignment = a.getInt(R.styleable.TwoLinesTextView_textAlignment, 0);
        } finally {
            a.recycle();
        }

        final LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.two_lines_textview_layout, this);
        getUIElements();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        final FontUtil fontUtil = FontUtil.get(getContext());
        getUIElements();
        mLowerTextView.setTypeface(fontUtil.getRobotoRegularTypeface());

        mUpperTextView.setTypeface(fontUtil.getRobotoMediumTypeface());
        mUpperTextView.setTextColor(mUpperTextColor);
        mLowerTextView.setTextColor(mLowerTextColor);
        mUpperTextView.setTextColor(mUpperTextColor);
        if (mLowerTextSize != NO_TEXT_SIZE) {
            mLowerTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mLowerTextSize);
        }
        if (mUpperTextSize != NO_TEXT_SIZE) {
            mUpperTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mUpperTextSize);
        }
        if (mTextAlignment != 0) {
            mLowerTextView.setTextAlignment(TEXT_ALIGNMENT_TEXT_END);
            mUpperTextView.setTextAlignment(TEXT_ALIGNMENT_TEXT_END);
        } else {
            mLowerTextView.setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
            mUpperTextView.setTextAlignment(TEXT_ALIGNMENT_TEXT_START);
        }
        if (isInEditMode()) {
            mUpperTextView.setText("Upper String");
            mLowerTextView.setText("Lower String");
        }
    }

    public void setUpperText(String upperText) {
        mUpperTextView.setText(upperText);
    }

    public void setLowerText(String upperText) {
        mLowerTextView.setText(upperText);
    }

    public void setUpperTextColor(int upperTextColor) {
        mUpperTextView.setTextColor(upperTextColor);
    }

    public void setLowerTextColor(int lowerTextColor) {
        mLowerTextView.setTextColor(lowerTextColor);
    }

    public void setUpperTextSize(float upperTextSize) {
        mUpperTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, upperTextSize);
    }

    public void setLowerTextSize(float lowerTextSize) {
        mLowerTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, lowerTextSize);
    }

    private final void getUIElements() {
        mUpperTextView = UI.findViewById(this, R.id.upperTextView);
        mLowerTextView = UI.findViewById(this, R.id.lowerTextView);
    }
}
