package uk.co.example.skyscannertest.business;

import android.support.annotation.Nullable;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;

import uk.co.example.skyscannertest.business.model.Agent;
import uk.co.example.skyscannertest.business.model.Carrier;
import uk.co.example.skyscannertest.business.model.Leg;
import uk.co.example.skyscannertest.business.model.Place;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.Segment;

/**
 * Implementation for the SkyScanRepository
 */

public class SkyScanRepositoryImpl implements SkyScanRepository {

    private Map<Integer, Agent> mAgentDb = new ConcurrentHashMap<>();

    private Map<String, Leg> mLegsDb = new ConcurrentHashMap<>();

    private Map<Integer, Segment> mSegmentsDb = new ConcurrentHashMap<>();

    private Map<Integer, Place> mPlacesDb = new ConcurrentHashMap<>();

    private Map<Integer, Carrier> mCarriersDb = new ConcurrentHashMap<>();

    @Inject
    public SkyScanRepositoryImpl() {
    }

    @Override
    public void loadData(PollingResult pollingResult) {
        if (pollingResult != null) {
            // We update the Agent Db
            final Map<Integer, Agent> newAgentDb = new ConcurrentHashMap<>();
            final List<Agent> agents = pollingResult.getAgents();
            if (agents != null) {
                for (Agent agent : agents) {
                    newAgentDb.put(agent.getId(), agent);
                }
                // Replace the existing
                mAgentDb = newAgentDb;
            }
            // We update the Legs
            final Map<String, Leg> newLegsDb = new ConcurrentHashMap<>();
            final List<Leg> legs = pollingResult.getLegs();
            if (legs != null) {
                for (Leg leg : legs) {
                    newLegsDb.put(leg.getId(), leg);
                }
                // Replace the existing
                mLegsDb = newLegsDb;
            }
            // The Segments
            final Map<Integer, Segment> newSegmentsDb = new ConcurrentHashMap<>();
            final List<Segment> segments = pollingResult.getSegments();
            if (segments != null) {
                for (Segment segment : segments) {
                    newSegmentsDb.put(segment.getId(), segment);
                }
                // Replace the existing
                mSegmentsDb = newSegmentsDb;
            }
            // The Places
            final Map<Integer, Place> newPlacesDb = new ConcurrentHashMap<>();
            final List<Place> places = pollingResult.getPlaces();
            if (places != null) {
                for (Place place : places) {
                    newPlacesDb.put(place.getId(), place);
                }
                // Replace the existing
                mPlacesDb = newPlacesDb;
            }
            // Carriers
            final Map<Integer, Carrier> newCarriersDb = new ConcurrentHashMap<>();
            final List<Carrier> carriers = pollingResult.getCarriers();
            if (carriers != null) {
                for (Carrier carrier : carriers) {
                    newCarriersDb.put(carrier.getId(), carrier);
                }
                // Replace the existing
                mCarriersDb = newCarriersDb;
            }
        }
    }

    @Nullable
    @Override
    public Agent findAgentById(int agentId) {
        return mAgentDb.get(agentId);
    }

    @Nullable
    @Override
    public Leg findLegById(String legId) {
        return mLegsDb.get(legId);
    }

    @Nullable
    @Override
    public Segment findSegmentById(int segmentId) {
        return mSegmentsDb.get(segmentId);
    }

    @Nullable
    @Override
    public Place findPlaceById(int placeId) {
        return mPlacesDb.get(placeId);
    }

    @Nullable
    @Override
    public Carrier findCarrierById(int carrierId) {
        return mCarriersDb.get(carrierId);
    }
}
