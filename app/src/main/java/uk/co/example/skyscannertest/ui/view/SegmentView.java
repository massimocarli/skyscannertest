package uk.co.example.skyscannertest.ui.view;

import android.content.Context;
import android.support.v4.util.Pools;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.UI;
import uk.co.example.skyscannertest.ui.search.SegmentViewModel;

/**
 * A compound custom view for the single Segment
 */

public class SegmentView extends LinearLayout {

    private static final Pools.Pool<SegmentView> sPool = new Pools.SimplePool<SegmentView>(20);

    public static SegmentView obtain(Context context) {
        SegmentView instance = sPool.acquire();
        return (instance != null) ? instance : new SegmentView(context);
    }

    public void recycle() {
        // Clear state if needed.
        sPool.release(this);
    }

    private TwoLinesTextView mFlightInfo;
    private TwoLinesTextView mFlightDuration;
    private AppCompatImageView mFlightIcon;

    public SegmentView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        setOrientation(LinearLayout.HORIZONTAL);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.segment_layout, this);
        mFlightInfo = UI.findViewById(view, R.id.segmentFlightInfo);
        mFlightDuration = UI.findViewById(view, R.id.segmentFlightDuration);
        mFlightIcon = UI.findViewById(view, R.id.segmentImage);
    }

    /**
     * Display the given ViewModel data into the compoennt
     */
    public void setSegmentViewModel(SegmentViewModel segmentViewModel) {
        mFlightInfo.setUpperText(segmentViewModel.flightInfoTime);
        mFlightInfo.setLowerText(segmentViewModel.flightInfoAirports);
        mFlightDuration.setUpperText(segmentViewModel.mFlightDurationType);
        mFlightDuration.setLowerText(segmentViewModel.mFlightDurationValue);
        Picasso.with(getContext())
                .load(segmentViewModel.mCarrierIconUrl)
                //.placeholder(R.drawable.user_placeholder)
                //.error(R.drawable.user_placeholder_error)
                .into(mFlightIcon);
    }
}
