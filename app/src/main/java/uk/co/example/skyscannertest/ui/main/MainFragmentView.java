package uk.co.example.skyscannertest.ui.main;

import java.util.Date;

import uk.co.example.skyscannertest.ui.common.mvp.View;

/**
 * The View for the MainFragment
 */

public interface MainFragmentView extends View {

    void launchResultFragment(Date outboundDate, Date inboundDate);
}
