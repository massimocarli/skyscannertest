package uk.co.example.skyscannertest.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.BaseActivity;
import uk.co.example.skyscannertest.ui.main.MainFragment;
import uk.co.example.skyscannertest.ui.search.SearchFragmentPresenter;
import uk.co.example.skyscannertest.ui.search.SearchFragmentView;

public class MainActivity extends BaseActivity<SearchFragmentPresenter, SearchFragmentView> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            replaceFragmentAt(R.id.anchor_point, MainFragment.newInstance());
        }
    }

    @Override
    protected void onStart() {
        getComp().inject(this);
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
