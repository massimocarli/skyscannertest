package uk.co.example.skyscannertest.ui.view;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.FontUtil;
import uk.co.example.skyscannertest.ui.common.util.UI;
import uk.co.example.skyscannertest.ui.search.AgentViewModel;

/**
 * A compound custom view for the single Agent information
 */

public class AgentView extends LinearLayout {

    private static final String SINGLE_DECIMAL_FORMAT = "%.1f";

    private AppCompatImageView mSatisfactionImageView;
    private TextView mSatisfactionValue;
    private TwoLinesTextView mAgentPrice;

    public AgentView(Context context) {
        super(context);
        init(context);
    }

    public AgentView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AgentView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setOrientation(LinearLayout.HORIZONTAL);
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.agent_layout, this);
        mSatisfactionImageView = UI.findViewById(view, R.id.satisfactionLevelImage);
        mSatisfactionValue = UI.findViewById(view, R.id.satisfactionLevel);
        mSatisfactionValue.setTypeface(FontUtil.get(getContext()).getRobotoMediumTypeface());
        mAgentPrice = UI.findViewById(view, R.id.agentPrice);
    }

    /**
     * Display the given ViewModel data into the component
     */
    public void setAgentViewModel(AgentViewModel agentViewModel) {
        final String rateValueAsString = String.format(SINGLE_DECIMAL_FORMAT, agentViewModel.agentRateValue);
        mSatisfactionValue.setText(rateValueAsString);
        mAgentPrice.setUpperText(agentViewModel.agentPrice);
        mAgentPrice.setLowerText(agentViewModel.agentName);
        mSatisfactionImageView.setImageResource(getSatisfactionDrawable(agentViewModel.agentRateValue));
    }

    @DrawableRes
    private int getSatisfactionDrawable(float satisfactionValue) {
        if (satisfactionValue < 0.2) {
            return R.drawable.ic_very_dissatisfied;
        } else if (satisfactionValue < 0.4) {
            return R.drawable.ic_dissatisfied;
        } else if (satisfactionValue < 0.6) {
            return R.drawable.ic_neutral;
        } else if (satisfactionValue < 0.8) {
            return R.drawable.ic_satisfied;
        } else {
            return R.drawable.ic_very_satisfied;
        }
    }
}
