package uk.co.example.skyscannertest.business.model;

import java.util.Date;

/**
 * This encapsulate the information about the Flight Request
 */

public class SearchRequest {

    private static final String API_KEY = "ss630745725358065467897349852985";

    public final String cabinClass;
    public final String country;
    public final String currency;
    public final String locale;
    public final String locationSchema;
    public final String originPlace;
    public final String destinationPlace;
    public final String originPlaceName;
    public final String destinationPlaceName;
    public final Date outboundDate;
    public final Date inboundDate;
    public final int adults;
    public final int children;
    public final int infants;
    public final int pageIndex;
    public final int pageSize;
    public final String apiKey = API_KEY;


    private SearchRequest(Builder builder) {
        cabinClass = builder.mCabinClass;
        country = builder.mCountry;
        currency = builder.mCurrency;
        locale = builder.mLocale;
        locationSchema = builder.mLocationSchema;
        originPlace = builder.mOriginPlace;
        destinationPlace = builder.mDestinationPlace;
        originPlaceName = builder.mOriginPlaceName;
        destinationPlaceName = builder.mDestinationPlaceName;
        outboundDate = builder.mOutboundDate;
        inboundDate = builder.mInboundDate;
        adults = builder.mAdults;
        children = builder.mChildren;
        infants = builder.mInfants;
        pageIndex = builder.mPageIndex;
        pageSize = builder.mPageSize;
    }


    public static class Builder {

        private String mCabinClass = "Economy";
        private String mCountry = "UK";
        private String mCurrency = "GBP";
        private String mLocale = "en-GB";
        private String mLocationSchema = "iata";
        private String mOriginPlace = "EDI";
        private String mDestinationPlace = "LHR";
        private String mOriginPlaceName = "Edinburgh";
        private String mDestinationPlaceName = "London";
        private final Date mOutboundDate;
        private final Date mInboundDate;
        private int mAdults = 1;
        private int mChildren = 0;
        private int mInfants = 0;
        private int mPageIndex = 0;
        private int mPageSize = 10;

        private Builder(Date outboundDate, Date inboundDate) {
            this.mOutboundDate = outboundDate;
            this.mInboundDate = inboundDate;
        }

        public static Builder create(Date outboundDate, Date inboundDate) {
            return new Builder(outboundDate, inboundDate);
        }

        public Builder withCabinClass(String cabinClass) {
            this.mCabinClass = cabinClass;
            return this;
        }

        public Builder withCountry(String country) {
            this.mCountry = country;
            return this;
        }

        public Builder withCurrency(String currency) {
            this.mCurrency = currency;
            return this;
        }

        public Builder withLocale(String locale) {
            this.mLocale = locale;
            return this;
        }

        public Builder withLocationSchema(String locationSchema) {
            this.mLocationSchema = locationSchema;
            return this;
        }

        public Builder withOriginPlace(String originPlace) {
            this.mOriginPlace = originPlace;
            return this;
        }

        public Builder withDestinationPlace(String destinationPlace) {
            this.mDestinationPlace = destinationPlace;
            return this;
        }

        public Builder withOriginPlaceName(String originPlaceName) {
            this.mOriginPlaceName = originPlaceName;
            return this;
        }

        public Builder withDestinationPlaceName(String destinationPlaceName) {
            this.mDestinationPlaceName = destinationPlaceName;
            return this;
        }

        public Builder withAdults(int adults) {
            this.mAdults = adults;
            return this;
        }

        public Builder withChildren(int children) {
            this.mChildren = children;
            return this;
        }

        public Builder withInfants(int infants) {
            this.mInfants = infants;
            return this;
        }

        public Builder withPageIndex(int pageIndex) {
            this.mPageIndex = pageIndex;
            return this;
        }

        public Builder withPageSize(int pageSize) {
            this.mPageSize = pageSize;
            return this;
        }

        public SearchRequest build() {
            return new SearchRequest(this);
        }
    }
}
