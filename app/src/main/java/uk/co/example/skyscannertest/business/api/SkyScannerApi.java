package uk.co.example.skyscannertest.business.api;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.SessionResponse;

/**
 * Abstraction for SkyScanner API
 */

public interface SkyScannerApi {

    @FormUrlEncoded
    @POST("pricing/v1.0")
    Observable<Response<SessionResponse>> startSession(@Field("cabinclass") String cabinClass,
                                                       @Field("country") String country,
                                                       @Field("currency") String currency,
                                                       @Field("locale") String locale,
                                                       @Field("locationSchema") String locationSchema,
                                                       @Field("originPlace") String originPlace,
                                                       @Field("destinationPlace") String destinationPlace,
                                                       @Field("outboundDate") String outboundDate,
                                                       @Field("inboundDate") String inboundDate,
                                                       @Field("adults") int adults,
                                                       @Field("children") int children,
                                                       @Field("infants") int infants,
                                                       @Field("pageIndex") int pageIndex,
                                                       @Field("pageSize") int pageSize,
                                                       @Field("apiKey") String apiKey);

    @GET("pricing/v1.0/{sessionKey}")
    Observable<PollingResult> pollData(@Path("sessionKey") String sessionKey,
                                       @Query("apiKey") String apiKey);
}
