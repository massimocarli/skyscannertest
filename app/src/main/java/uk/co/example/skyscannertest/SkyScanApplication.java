package uk.co.example.skyscannertest;

import android.app.Application;

import uk.co.example.skyscannertest.dagger.DaggerSkyScanComponent;
import uk.co.example.skyscannertest.dagger.NetworkModule;
import uk.co.example.skyscannertest.dagger.PresenterModule;
import uk.co.example.skyscannertest.dagger.RepositoryModule;
import uk.co.example.skyscannertest.dagger.ResourceModule;
import uk.co.example.skyscannertest.dagger.SkyScanComponent;

/**
 * Specific Application implementation with the role of Main component. It's responsible to
 * initialize frameworks and DI
 */

public class SkyScanApplication extends Application {

    private SkyScanComponent mSkyScanComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mSkyScanComponent = DaggerSkyScanComponent
                .builder()
                .networkModule(new NetworkModule())
                .presenterModule(new PresenterModule())
                .repositoryModule(new RepositoryModule())
                .resourceModule(new ResourceModule(this))
                .build();
    }

    /**
     * @return The reference to the SkyScan component
     */
    public SkyScanComponent getSkyScanComponent() {
        return mSkyScanComponent;
    }
}
