package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model for Agent
 */

public class Agent {

    @SerializedName("Id")
    private int mId;

    @SerializedName("Name")
    private String mName;

    @SerializedName("ImageUrl")
    private String mImageUrl;

    @SerializedName("Status")
    private String mStatus;

    @SerializedName("OptimisedForMobile")
    private boolean mOptimisedForMobile;

    @SerializedName("BookingNumber")
    private String mBookingNumber;

    @SerializedName("Type")
    private String mType;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public boolean isOptimisedForMobile() {
        return mOptimisedForMobile;
    }

    public void setOptimisedForMobile(boolean mOptimisedForMobile) {
        this.mOptimisedForMobile = mOptimisedForMobile;
    }

    public String getBookingNumber() {
        return mBookingNumber;
    }

    public void setBookingNumber(String mBookingNumber) {
        this.mBookingNumber = mBookingNumber;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }
}
