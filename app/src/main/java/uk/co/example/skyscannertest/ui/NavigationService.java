package uk.co.example.skyscannertest.ui;

import android.support.v4.app.Fragment;

/**
 * Abstraction for the services provided by a View about navigation
 */

public interface NavigationService {

    /**
     * This replace what in the given anchor with the new
     *
     * @param anchorId The anchor where to replace the Fragment
     * @param fragment The Fragment to put in the anchor place
     */
    void replaceFragmentAt(int anchorId, Fragment fragment);

    /**
     * This replace what in the given anchor with the new
     *
     * @param anchorId       The anchor where to replace the Fragment
     * @param fragment       The Fragment to put in the anchor place
     * @param addToBackStack If true we add the Fragment to the backstack using null
     */
    void replaceFragmentAt(int anchorId, Fragment fragment, boolean addToBackStack);

    /**
     * This replace what in the given anchor with the new
     *
     * @param anchorId  The anchor where to replace the Fragment
     * @param fragment  The Fragment to put in the anchor place
     * @param backStack The value to use for the backStack
     */
    void replaceFragmentAt(int anchorId, Fragment fragment, String backStack);

    /**
     * Pop from the BackStack
     */
    void popBackStack();
}
