package uk.co.example.skyscannertest.ui.main;

import java.util.Date;

import uk.co.example.skyscannertest.ui.common.mvp.Presenter;

/**
 * The Presenter for the MainFragment
 */

public interface MainFragmentPresenter extends Presenter<MainFragmentView> {

    /**
     * We start the search
     *
     * @param outboundDate The date for the inbound flight
     * @param inboundDate  The date for the outbound flight
     */
    void startSearchButtonPressed(Date outboundDate, Date inboundDate);
}
