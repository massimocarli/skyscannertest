package uk.co.example.skyscannertest.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * The Module which provides reference to Android objects
 */
@Module
public class ResourceModule {

    private final Context mContext;

    public ResourceModule(Context context) {
        this.mContext = context;
    }


    @Provides
    public Context provideContext() {
        return mContext;
    }
}
