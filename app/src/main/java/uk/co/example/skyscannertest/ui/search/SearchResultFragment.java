package uk.co.example.skyscannertest.ui.search;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.business.model.SearchRequest;
import uk.co.example.skyscannertest.ui.common.BaseFragment;
import uk.co.example.skyscannertest.ui.common.util.FontUtil;
import uk.co.example.skyscannertest.ui.common.util.UI;
import uk.co.example.skyscannertest.ui.common.view.ErrorMessageDialogFragment;
import uk.co.example.skyscannertest.ui.common.view.ProgressDialogFragment;
import uk.co.example.skyscannertest.ui.view.TwoLinesTextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class SearchResultFragment extends BaseFragment<SearchFragmentPresenter, SearchFragmentView> implements SearchFragmentView {

    private static final String PROGRESS_DIALOG_TAG = "PROGRESS_DIALOG_TAG";
    private static final String ERROR_DIALOG_TAG = "ERROR_DIALOG_TAG";

    private static final String INBOUND_EXTRA_NAME = "INBOUND_DATE";
    private static final String OUTBOUND_EXTRA_NAME = "OUTBOUND_DATE";

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ItineraryAdapter mAdapter;
    private List<ItineraryViewModel> mModel = new LinkedList<>();
    private TextView mSearchAndFilterTextView;
    private TextView mResultStateTextView;
    private ProgressDialogFragment mProgressDialog;
    private ErrorMessageDialogFragment mErrorMessageDialogFragment;
    private boolean mRequestInProgress;

    public SearchResultFragment() {
    }

    public static SearchResultFragment newInstance(Date outboundDate, Date inboundDate) {
        SearchResultFragment fragment = new SearchResultFragment();
        final Bundle args = new Bundle();
        args.putLong(OUTBOUND_EXTRA_NAME, outboundDate.getTime());
        args.putLong(INBOUND_EXTRA_NAME, inboundDate.getTime());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context) {
        getComp().inject(this);
        super.onAttach(context);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search_result, menu);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FontUtil fontUtil = FontUtil.get(getContext());
        mSearchAndFilterTextView = UI.findViewById(view, R.id.searchAndFilter);
        mSearchAndFilterTextView.setTypeface(fontUtil.getRobotoRegularTypeface());
        mResultStateTextView = UI.findViewById(view, R.id.resultState);
        mResultStateTextView.setTypeface(fontUtil.getRobotoRegularTypeface());

        mRecyclerView = UI.findViewById(view, R.id.my_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ItineraryAdapter(mModel);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mRequestInProgress) {
            startFlightSearch();
            mRequestInProgress = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getActionBar().setCustomView(null);
        getActionBar().setTitle(R.string.app_name);
    }

    @Override
    public void updateItineraryList(List<ItineraryViewModel> itineraryViewModels) {
        mModel.clear();
        mModel.addAll(itineraryViewModels);
        mAdapter.notifyDataSetChanged();

        // Is the total number always the same as the number of available results?
        final int size = itineraryViewModels.size();
        mResultStateTextView.setText(getString(R.string.flight_detail_numbers_pattern, size, size));

        dismissProgressDialog();
    }

    @Override
    public void displayError(Throwable throwable) {
        final String message = getString(R.string.error_message_format, throwable.getLocalizedMessage());
        getErrorDialog(message).show(getChildFragmentManager(), ERROR_DIALOG_TAG);
    }

    @Override
    public void displayProgressDialog() {
        getProgressDialog().show(getChildFragmentManager(), PROGRESS_DIALOG_TAG);
    }

    @Override
    public void hideProgressDialog() {
        mProgressDialog.dismiss();
    }

    @Override
    public void searchCompleted() {
        mRequestInProgress = false;
    }

    @Override
    public void updateSearchTitle(ActionBarViewModel searchTitleModel) {
        final Resources resources = getContext().getResources();
        final TwoLinesTextView searchTitle = new TwoLinesTextView(getContext());

        searchTitle.setUpperTextSize(resources.getDimension(R.dimen.upper_action_bar_text_size));
        searchTitle.setUpperTextColor(resources.getColor(R.color.upperTitleLineColor));
        searchTitle.setUpperText(searchTitleModel.flightDestination);

        searchTitle.setLowerTextSize(resources.getDimension(R.dimen.lower_action_bar_text_size));
        searchTitle.setLowerTextColor(resources.getColor(R.color.lowerTitleLineColor));
        searchTitle.setLowerText(searchTitleModel.flightRequestInfo);


        final ActionBar actionBar = getActionBar();
        actionBar.setTitle(null);
        actionBar.setCustomView(searchTitle);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    private Date getDateFromArguments(String extraName) {
        final Date date = new Date();
        date.setTime(getArguments().getLong(extraName));
        return date;
    }

    private void dismissProgressDialog() {
        mProgressDialog.dismiss();
    }

    private ProgressDialogFragment getProgressDialog() {
        mProgressDialog = ProgressDialogFragment.getInstance();
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dismissProgressDialog();
                getNavigationService().popBackStack();
            }
        });
        return mProgressDialog;
    }

    private ErrorMessageDialogFragment getErrorDialog(String message) {
        mErrorMessageDialogFragment = mErrorMessageDialogFragment.getInstance(message);
        mErrorMessageDialogFragment.setOnErrorMessageDialogListener(
                new ErrorMessageDialogFragment.OnErrorMessageDialogListener() {
                    @Override
                    public void onErrorMessageResponse(boolean continueOrNot) {
                        if (continueOrNot) {
                            startFlightSearch();
                        } else {
                            getNavigationService().popBackStack();
                        }
                    }
                });
        return mErrorMessageDialogFragment;
    }

    private void startFlightSearch() {
        // We read the input time
        final SearchRequest searchRequest = SearchRequest.Builder
                .create(getDateFromArguments(OUTBOUND_EXTRA_NAME), getDateFromArguments(INBOUND_EXTRA_NAME))
                .build();
        mPresenter.searchForFlight(searchRequest);
    }
}
