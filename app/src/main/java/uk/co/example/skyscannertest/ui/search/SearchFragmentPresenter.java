package uk.co.example.skyscannertest.ui.search;

import java.util.Date;

import uk.co.example.skyscannertest.business.model.SearchRequest;
import uk.co.example.skyscannertest.ui.common.mvp.Presenter;

/**
 * Interface for the SearchFragmentPresenter
 */

public interface SearchFragmentPresenter extends Presenter<SearchFragmentView> {

    void searchForFlight(SearchRequest searchRequest);
}
