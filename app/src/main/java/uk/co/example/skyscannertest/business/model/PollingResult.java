package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * This encapsulates information from a polling request for a given session
 */

public class PollingResult {

    private static final String STATUS_UPDATES_COMPLETE = "UpdatesComplete";

    @SerializedName("Status")
    String mStatus;

    @SerializedName("Segments")
    List<Segment> segments;

    @SerializedName("Currencies")
    List<Currency> currencies;

    @SerializedName("Places")
    List<Place> places;

    @SerializedName("Agents")
    List<Agent> agents;

    @SerializedName("Carriers")
    List<Carrier> carriers;

    @SerializedName("Legs")
    List<Leg> legs;

    @SerializedName("Itineraries")
    List<Itinerary> itineraries;


    public boolean isUpdatesCompleted() {
        return STATUS_UPDATES_COMPLETE.equals(mStatus);
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public List<Place> getPlaces() {
        return places;
    }

    public List<Agent> getAgents() {
        return agents;
    }

    public List<Carrier> getCarriers() {
        return carriers;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public List<Itinerary> getItineraries() {
        return itineraries;
    }
}
