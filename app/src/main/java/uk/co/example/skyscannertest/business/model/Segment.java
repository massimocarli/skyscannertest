package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Data about Segment
 */
public class Segment {

    @SerializedName("Id")
    private int mId;

    @SerializedName("OriginStation")
    private int mOriginStation;

    @SerializedName("DestinationStation")
    private int mDestinationStation;

    @SerializedName("DepartureDateTime")
    private Date mDepartureDateTime;

    @SerializedName("ArrivalDateTime")
    private Date mArrivalDateTime;

    @SerializedName("Carrier")
    private int mCarrier;

    @SerializedName("OperatingCarrier")
    private int mOperatingCarrier;

    @SerializedName("Duration")
    private int mDuration;

    @SerializedName("FlightNumber")
    private String mFlightNumber;

    @SerializedName("JourneyMode")
    private String mJourneyMode;

    @SerializedName("Directionality")
    private String mDirectionality;

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public int getOriginStation() {
        return mOriginStation;
    }

    public void setOriginStation(int mOriginStation) {
        this.mOriginStation = mOriginStation;
    }

    public int getDestinationStation() {
        return mDestinationStation;
    }

    public void setDestinationStation(int mDestinationStation) {
        this.mDestinationStation = mDestinationStation;
    }

    public Date getDepartureDateTime() {
        return mDepartureDateTime;
    }

    public void setDepartureDateTime(Date mDepartureDateTime) {
        this.mDepartureDateTime = mDepartureDateTime;
    }

    public Date getArrivalDateTime() {
        return mArrivalDateTime;
    }

    public void setArrivalDateTime(Date mArrivalDateTime) {
        this.mArrivalDateTime = mArrivalDateTime;
    }

    public int getCarrier() {
        return mCarrier;
    }

    public void setCarrier(int mCarrier) {
        this.mCarrier = mCarrier;
    }

    public int getOperatingCarrier() {
        return mOperatingCarrier;
    }

    public void setOperatingCarrier(int mOperatingCarrier) {
        this.mOperatingCarrier = mOperatingCarrier;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public String getFlightNumber() {
        return mFlightNumber;
    }

    public void setFlightNumber(String mFlightNumber) {
        this.mFlightNumber = mFlightNumber;
    }

    public String getJourneyMode() {
        return mJourneyMode;
    }

    public void setJourneyMode(String mJourneyMode) {
        this.mJourneyMode = mJourneyMode;
    }

    public String getDirectionality() {
        return mDirectionality;
    }

    public void setDirectionality(String mDirectionality) {
        this.mDirectionality = mDirectionality;
    }
}
