package uk.co.example.skyscannertest.ui.common.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.UI;

/**
 * Utility class for a simple ProgressDialog
 */

public class ErrorMessageDialogFragment extends DialogFragment {

    private static final String ERROR_MESSAGE_EXTRA = "ERROR_MESSAGE_EXTRA";

    public interface OnErrorMessageDialogListener {

        /**
         * Used to notify the user decision after an error
         *
         * @param continueOrNot If true the user wants to continue
         */
        void onErrorMessageResponse(boolean continueOrNot);
    }

    private OnErrorMessageDialogListener mOnErrorMessageDialogListener;

    public static ErrorMessageDialogFragment getInstance(String errorMessage) {
        final ErrorMessageDialogFragment fragment = new ErrorMessageDialogFragment();
        final Bundle args = new Bundle();
        args.putString(ERROR_MESSAGE_EXTRA, errorMessage);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setOnErrorMessageDialogListener(OnErrorMessageDialogListener onErrorMessageDialogListener) {
        this.mOnErrorMessageDialogListener = onErrorMessageDialogListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String defaultErrorMessage = getString(R.string.error_message);
        final String errorMessage = getArguments().getString(ERROR_MESSAGE_EXTRA, defaultErrorMessage);
        final View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.error_dialog_layout, null);
        final TextView messageTextView = UI.findViewById(dialogView, R.id.errorMessage);
        messageTextView.setText(errorMessage);

        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.error_title)
                .setView(dialogView)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mOnErrorMessageDialogListener != null) {
                            mOnErrorMessageDialogListener.onErrorMessageResponse(true);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mOnErrorMessageDialogListener != null) {
                            mOnErrorMessageDialogListener.onErrorMessageResponse(false);
                        }
                    }
                })
                .create();
    }

    /**
     * Fix for bug on rotation (https://stackoverflow.com/a/12434038)
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }
}

