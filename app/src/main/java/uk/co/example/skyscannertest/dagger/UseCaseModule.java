package uk.co.example.skyscannertest.dagger;

import dagger.Module;
import dagger.Provides;
import uk.co.example.skyscannertest.business.SkyScanRepository;
import uk.co.example.skyscannertest.business.api.SkyScannerApi;
import uk.co.example.skyscannertest.business.search.SearchUseCase;
import uk.co.example.skyscannertest.business.search.SearchUseCaseImpl;

/**
 * The Module for UseCases
 */

@Module
public class UseCaseModule {

    @Provides
    public SearchUseCase provideSearchUseCaseImpl(SkyScannerApi skyScannerApi,
                                                  SkyScanRepository skyScanRepository) {
        return new SearchUseCaseImpl(skyScannerApi, skyScanRepository);
    }
}
