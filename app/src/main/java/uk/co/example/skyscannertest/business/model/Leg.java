package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Model class for a Leg
 */

public class Leg {

    static class FlightInfo {

        @SerializedName("FlightNumber")
        private String mFlightNumber;

        @SerializedName("CarrierId")
        private int mCarrierId;
    }

    @SerializedName("Id")
    private String mId;

    @SerializedName("SegmentIds")
    private int[] mSegmentIds;

    @SerializedName("OriginStation")
    private int mOriginStation;

    @SerializedName("DestinationStation")
    private int mDestinationStation;

    @SerializedName("Departure")
    private Date mDeparture;

    @SerializedName("Arrival")
    private Date mArrival;

    @SerializedName("Duration")
    private int mDuration;

    @SerializedName("JourneyMode")
    private String mJourneyMode;

    @SerializedName("Stops")
    private int[] mStops;

    @SerializedName("Carriers")
    private int[] mCarriers;

    @SerializedName("OperatingCarriers")
    private int[] mOperatingCarriers;

    @SerializedName("Directionality")
    private String mDirectionality;

    @SerializedName("FlightNumbers")
    private FlightInfo[] mFlightNumbers;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public int[] getSegmentIds() {
        return mSegmentIds;
    }

    public void setSegmentIds(int[] mSegmentIds) {
        this.mSegmentIds = mSegmentIds;
    }

    public int getOriginStation() {
        return mOriginStation;
    }

    public void setOriginStation(int mOriginStation) {
        this.mOriginStation = mOriginStation;
    }

    public int getDestinationStation() {
        return mDestinationStation;
    }

    public void setDestinationStation(int mDestinationStation) {
        this.mDestinationStation = mDestinationStation;
    }

    public Date getDeparture() {
        return mDeparture;
    }

    public void setDeparture(Date mDeparture) {
        this.mDeparture = mDeparture;
    }

    public Date getArrival() {
        return mArrival;
    }

    public void setArrival(Date mArrival) {
        this.mArrival = mArrival;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int mDuration) {
        this.mDuration = mDuration;
    }

    public String getJourneyMode() {
        return mJourneyMode;
    }

    public void setJourneyMode(String mJourneyMode) {
        this.mJourneyMode = mJourneyMode;
    }

    public int[] getStops() {
        return mStops;
    }

    public void setStops(int[] mStops) {
        this.mStops = mStops;
    }

    public int[] getCarriers() {
        return mCarriers;
    }

    public void setCarriers(int[] mCarriers) {
        this.mCarriers = mCarriers;
    }

    public int[] getOperatingCarriers() {
        return mOperatingCarriers;
    }

    public void setOperatingCarriers(int[] mOperatingCarriers) {
        this.mOperatingCarriers = mOperatingCarriers;
    }

    public String getDirectionality() {
        return mDirectionality;
    }

    public void setDirectionality(String mDirectionality) {
        this.mDirectionality = mDirectionality;
    }

    public FlightInfo[] getFlightNumbers() {
        return mFlightNumbers;
    }

    public void setFlightNumbers(FlightInfo[] mFlightNumbers) {
        this.mFlightNumbers = mFlightNumbers;
    }
}
