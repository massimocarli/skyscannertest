package uk.co.example.skyscannertest.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import uk.co.example.skyscannertest.business.SkyScanRepository;
import uk.co.example.skyscannertest.ui.search.SearchFragmentPresenter;
import uk.co.example.skyscannertest.ui.search.SearchFragmentPresenterImpl;
import uk.co.example.skyscannertest.ui.main.MainFragmentPresenter;
import uk.co.example.skyscannertest.ui.main.MainFragmentPresenterImpl;
import uk.co.example.skyscannertest.business.search.SearchUseCase;

/**
 * This is the Module for all the Presenters
 */

@Module
public class PresenterModule {

    @Provides
    public MainFragmentPresenter provideMainFragmentPresenter() {
        return new MainFragmentPresenterImpl();
    }

    @Provides
    public SearchFragmentPresenter provideSearchFragmentPresenter(SkyScanRepository skyScanRepository,
                                                                  SearchUseCase searchUseCase,
                                                                  Context context) {
        return new SearchFragmentPresenterImpl(skyScanRepository, searchUseCase, context);
    }
}
