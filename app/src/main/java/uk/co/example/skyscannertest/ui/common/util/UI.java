package uk.co.example.skyscannertest.ui.common.util;

import android.view.View;

/**
 * Utility class for UI lookup
 */

public class UI {

    private UI() {
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(View containerView, int viewId) {
        // We find the view with the given Id
        View foundView = containerView.findViewById(viewId);
        // We return the View with the given cast
        return (T) foundView;
    }
}
