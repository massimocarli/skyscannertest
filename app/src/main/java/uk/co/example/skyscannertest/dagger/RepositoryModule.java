package uk.co.example.skyscannertest.dagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.example.skyscannertest.business.SkyScanRepository;
import uk.co.example.skyscannertest.business.SkyScanRepositoryImpl;

/**
 * The Module for Repository
 */

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public SkyScanRepository provideSkyScanRepository() {
        return new SkyScanRepositoryImpl();
    }
}
