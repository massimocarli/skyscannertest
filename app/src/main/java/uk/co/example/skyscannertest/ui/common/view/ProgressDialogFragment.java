package uk.co.example.skyscannertest.ui.common.view;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.UI;

/**
 * Utility class for a simple ProgressDialog
 */

public class ProgressDialogFragment extends DialogFragment {

    private DialogInterface.OnCancelListener mOnCancelListener;

    public static ProgressDialogFragment getInstance() {
        final ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setCancelable(true);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        this.mOnCancelListener = onCancelListener;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        mOnCancelListener.onCancel(dialog);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.loading_dialog_layout, null);
        final TextView labelTextView = UI.findViewById(dialogView, R.id.loadingDialogText);
        labelTextView.setText(R.string.please_wait_label);
        return new AlertDialog.Builder(getContext())
                .setView(dialogView)
                .setTitle(R.string.searching_flights_label)
                .create();
    }

    /**
     * Fix for bug on rotation (https://stackoverflow.com/a/12434038)
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }
}

