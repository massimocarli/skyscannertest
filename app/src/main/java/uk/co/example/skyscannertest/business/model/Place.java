package uk.co.example.skyscannertest.business.model;

import com.google.gson.annotations.SerializedName;

/**
 * Model for place
 */

public class Place {

    @SerializedName("Id")
    private int mId;

    @SerializedName("ParentId")
    private int mParentId;

    @SerializedName("Code")
    private String mCode;

    @SerializedName("Type")
    private String mType;

    @SerializedName("Name")
    private String mName;

    public int getId() {
        return mId;
    }

    public int getParentId() {
        return mParentId;
    }

    public String getCode() {
        return mCode;
    }

    public String getType() {
        return mType;
    }

    public String getName() {
        return mName;
    }
}
