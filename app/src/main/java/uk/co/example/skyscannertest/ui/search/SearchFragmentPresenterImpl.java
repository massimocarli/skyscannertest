package uk.co.example.skyscannertest.ui.search;

import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.business.SkyScanRepository;
import uk.co.example.skyscannertest.business.model.Carrier;
import uk.co.example.skyscannertest.business.model.Itinerary;
import uk.co.example.skyscannertest.business.model.Leg;
import uk.co.example.skyscannertest.business.model.Place;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.SearchRequest;
import uk.co.example.skyscannertest.business.model.Segment;
import uk.co.example.skyscannertest.business.search.SearchUseCase;
import uk.co.example.skyscannertest.ui.common.BasePresenter;
import uk.co.example.skyscannertest.ui.common.util.TimeUtil;

/**
 * The implementation for the SearchFragmentPresenter
 */

public class SearchFragmentPresenterImpl extends BasePresenter<SearchFragmentView> implements SearchFragmentPresenter {

    private final static Random sRandom = new Random();
    private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

    private final SkyScanRepository mSkyScanRepository;
    private final SearchUseCase mSearchUseCase;

    private final Context mContext;

    public SearchFragmentPresenterImpl(SkyScanRepository skyScanRepository,
                                       SearchUseCase searchUseCase,
                                       Context context) {
        this.mSkyScanRepository = skyScanRepository;
        this.mSearchUseCase = searchUseCase;
        this.mContext = context;
    }

    @Override
    public void searchForFlight(SearchRequest searchRequest) {
        if (isAttached()) {
            getView().displayProgressDialog();
            getView().updateSearchTitle(buildActionBarViewModel(searchRequest));
            final Disposable disposable = mSearchUseCase.searchFlight(searchRequest)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<PollingResult>() {
                        @Override
                        public void accept(PollingResult pollingResult) throws Exception {
                            sendResultToView(pollingResult);
                        }

                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            sendError(throwable);
                        }
                    });
            registerDisposable(disposable);
        }
    }

    private ActionBarViewModel buildActionBarViewModel(SearchRequest searchRequest) {
        final String flightPathTitle = mContext.getString(R.string.flight_path_title_pattern,
                searchRequest.originPlace,
                searchRequest.destinationPlaceName);

        final StringBuilder flightDetailsBuilder =
                new StringBuilder(mContext.getString(R.string.flight_detail_dates_pattern,
                        TimeUtil.asShortString(searchRequest.outboundDate),
                        TimeUtil.asShortString(searchRequest.inboundDate)));
        if (searchRequest.adults > 0) {
            flightDetailsBuilder.append(mContext.getString(R.string.flight_detail_adults_pattern,
                    searchRequest.adults));
        }
        if (searchRequest.children > 0) {
            flightDetailsBuilder.append(mContext.getString(R.string.flight_detail_children_pattern,
                    searchRequest.children));
        }
        if (searchRequest.infants > 0) {
            flightDetailsBuilder.append(mContext.getString(R.string.flight_detail_infants_pattern,
                    searchRequest.infants));
        }
        flightDetailsBuilder.append(mContext.getString(R.string.flight_detail_class_pattern,
                searchRequest.cabinClass).toLowerCase());

        return ActionBarViewModel.Builder
                .create()
                .withFlightDestination(flightPathTitle)
                .withFlightRequestInfo(flightDetailsBuilder.toString())
                .build();
    }

    private void sendResultToView(PollingResult pollingResult) {
        // Here we create the List of ItineraryView for the Itineraries
        final List<Itinerary> itineraries = pollingResult.getItineraries();
        final List<ItineraryViewModel> viewModelList = new ArrayList<>(itineraries.size());
        for (Itinerary itinerary : itineraries) {
            // Create the ItineraryViewModel
            final ItineraryViewModel itineraryViewModel = new ItineraryViewModel();
            // Get the Legs
            final Leg outboundLeg = mSkyScanRepository.findLegById(itinerary.getOutboundLegId());
            insertSegmentForLeg(itineraryViewModel, outboundLeg);
            final Leg inboundLeg = mSkyScanRepository.findLegById(itinerary.getInboundLegId());
            insertSegmentForLeg(itineraryViewModel, inboundLeg);
            viewModelList.add(itineraryViewModel);
            // The Agent and Price option
            final Itinerary.PricingOption[] pricingOptions = itinerary.getPricingOptions();
            if (pricingOptions != null && pricingOptions.length > 0) {
                final Itinerary.PricingOption pricingOption = pricingOptions[0];
                final String agentName = mSkyScanRepository.findAgentById(pricingOption.mAgents[0]).getName();
                final float rate = Math.abs(sRandom.nextInt(10)) / 10f;
                final AgentViewModel agentViewModel = AgentViewModel.Builder
                        .create()
                        .withAgentPrice(mContext.getString(R.string.agent_price_pattern, pricingOption.mPrice))
                        .withAgentName(mContext.getString(R.string.agent_name_pattern, agentName))
                        .withAgentRateValue(rate)
                        .build();
                itineraryViewModel.setAgentViewModel(agentViewModel);
            }
        }
        if (isAttached()) {
            getView().updateItineraryList(viewModelList);
            getView().hideProgressDialog();
            if (pollingResult.isUpdatesCompleted()) {
                getView().searchCompleted();
            }
        }
    }

    private void sendError(Throwable throwable) {
        if (isAttached()) {
            getView().hideProgressDialog();
            getView().displayError(throwable);
        }
    }


    private void insertSegmentForLeg(ItineraryViewModel itineraryViewModel, Leg leg) {
        final int[] segIds = leg.getSegmentIds();
        int stopNumber = 1;
        for (int segId : segIds) {
            final Segment segment = mSkyScanRepository.findSegmentById(segId);
            // The Departure and Arrival time
            final String departureTimeStr = TIME_FORMAT.format(segment.getDepartureDateTime());
            final String arrivalTimeStr = TIME_FORMAT.format(segment.getArrivalDateTime());
            final String flightTime = mContext.getString(R.string.departure_arrival_pattern, departureTimeStr, arrivalTimeStr);
            // The destination and arrivals
            final Place departurePlace = mSkyScanRepository.findPlaceById(segment.getOriginStation());
            final Place arrivalPlace = mSkyScanRepository.findPlaceById(segment.getDestinationStation());
            // The Carries
            final Carrier carrier = mSkyScanRepository.findCarrierById(segment.getCarrier());
            final String flightInfoAirports = mContext.getString(R.string.airports_carrier_pattern,
                    departurePlace.getCode(), arrivalPlace.getCode(), carrier.getName());
            // The type of flight depends on the number of stops in the Leg
            final String flightDurationType;
            if (leg.getStops() != null && leg.getStops().length > 0) {
                if (stopNumber < leg.getStops().length + 1) {
                    flightDurationType = mContext.getString(R.string.stop_number_pattern, stopNumber++);
                } else {
                    flightDurationType = mContext.getString(R.string.direct_flight_label);
                }
            } else {
                flightDurationType = mContext.getString(R.string.direct_flight_label);
            }
            // Duration
            final String flightDuration = TimeUtil.durationToString(segment.getDuration());
            // THe icon for the carrier
            final String mCarrierIconUrl = mContext.getString(R.string.carrier_icon_url_pattern, carrier.getCode());

            final SegmentViewModel segmentViewModel = SegmentViewModel.Builder.create()
                    .withFlightInfoTime(flightTime)
                    .withFlightInfoAirports(flightInfoAirports)
                    .withFlightDurationType(flightDurationType)
                    .withFlightDurationValue(flightDuration)
                    .withCarrierIconUrl(mCarrierIconUrl)
                    .build();
            itineraryViewModel.addSegment(segmentViewModel);
        }
    }
}
