package uk.co.example.skyscannertest.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import uk.co.example.skyscannertest.R;

/**
 * This is the Adapter to use in order to display the Itinerary
 */

public class ItineraryAdapter extends RecyclerView.Adapter<ItineraryViewHolder> {


    private final List<ItineraryViewModel> mModel;

    public ItineraryAdapter(List<ItineraryViewModel> model) {
        this.mModel = model;
    }


    @Override
    public ItineraryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Let's create the view based on the type
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itinerary_item_layout, parent, false);
        ItineraryViewHolder itineraryViewHolder = new ItineraryViewHolder(view);
        return itineraryViewHolder;
    }

    @Override
    public void onBindViewHolder(ItineraryViewHolder holder, int position) {
        holder.displayModel(mModel.get(position));
    }

    @Override
    public int getItemCount() {
        return mModel.size();
    }
}
