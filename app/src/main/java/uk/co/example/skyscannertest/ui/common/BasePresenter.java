package uk.co.example.skyscannertest.ui.common;

import android.support.annotation.CallSuper;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import uk.co.example.skyscannertest.ui.common.mvp.Presenter;
import uk.co.example.skyscannertest.ui.common.mvp.View;

/**
 * The base implementation for the Presenter. It manages View lifecycle
 */

public abstract class BasePresenter<V extends View> implements Presenter<V> {

    private V mView;

    /**
     * For Resource management
     */
    private CompositeDisposable compositeDisposable;

    private boolean mAttached;

    @Override
    @CallSuper
    public void attach(V v) {
        compositeDisposable = new CompositeDisposable();
        mView = v;
        mAttached = true;
    }

    @Override
    @CallSuper
    public void detach() {
        mAttached = false;
        mView = null;
        compositeDisposable.dispose();
    }

    protected void registerDisposable(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected V getView() {
        return mView;
    }

    public boolean isAttached() {
        return mAttached;
    }
}
