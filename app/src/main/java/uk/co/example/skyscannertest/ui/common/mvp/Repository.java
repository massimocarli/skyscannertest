package uk.co.example.skyscannertest.ui.common.mvp;

/**
 * Repository abstraction in clean architecture sense. Implementations of this interface will
 * contain access to data (network or persisted)
 */
public interface Repository {
}
