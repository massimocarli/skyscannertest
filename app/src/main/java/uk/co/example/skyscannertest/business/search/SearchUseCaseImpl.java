package uk.co.example.skyscannertest.business.search;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import uk.co.example.skyscannertest.business.SkyScanRepository;
import uk.co.example.skyscannertest.business.api.SkyScannerApi;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.SearchRequest;
import uk.co.example.skyscannertest.business.model.SessionResponse;
import uk.co.example.skyscannertest.ui.common.util.TimeUtil;

/**
 * Implementation for UseCase
 */

public class SearchUseCaseImpl implements SearchUseCase {

    static final String LOCATION_HEADER_NAME = "Location";

    private static final String API_KEY = "ss630745725358065467897349852985";
    private static final int MAX_RETRY_TIMES = 5;
    private static final int POLLING_DELAY_SEC = 2;

    private final SkyScannerApi mSkyScannerApi;
    private final SkyScanRepository mSkyScanRepository;

    public SearchUseCaseImpl(SkyScannerApi skyScannerApi, SkyScanRepository skyScanRepository) {
        mSkyScannerApi = skyScannerApi;
        mSkyScanRepository = skyScanRepository;
    }

    @Override
    public Observable<PollingResult> searchFlight(SearchRequest searchRequest) {
        return mSkyScannerApi
                .startSession(
                        searchRequest.cabinClass,
                        searchRequest.country,
                        searchRequest.currency,
                        searchRequest.locale,
                        searchRequest.locationSchema,
                        searchRequest.originPlace,
                        searchRequest.destinationPlace,
                        TimeUtil.asString(searchRequest.outboundDate),
                        TimeUtil.asString(searchRequest.inboundDate),
                        searchRequest.adults,
                        searchRequest.children,
                        searchRequest.infants,
                        searchRequest.pageIndex,
                        searchRequest.pageSize,
                        searchRequest.apiKey)
                .subscribeOn(Schedulers.io())
                .map(new Function<Response<SessionResponse>, String>() {
                    @Override
                    public String apply(Response<SessionResponse> sessionResponseResponse) throws Exception {
                        final String sessionKey = getSessionKey(sessionResponseResponse);
                        if (sessionKey == null) {
                            throw new IllegalStateException("Invalid SessionKey");
                        }
                        return sessionKey;
                    }
                })
                .flatMap(new Function<String, ObservableSource<PollingResult>>() {
                    @Override
                    public ObservableSource<PollingResult> apply(final String sessionKey) throws Exception {
                        return mSkyScannerApi.pollData(sessionKey, API_KEY)
                                .retry(MAX_RETRY_TIMES)
                                .repeatWhen(new Function<Observable<Object>, ObservableSource<?>>() {
                                    @Override
                                    public ObservableSource<?> apply(Observable<Object> objectObservable) throws Exception {
                                        return objectObservable.delay(POLLING_DELAY_SEC, TimeUnit.SECONDS);
                                    }
                                });
                    }
                })
                .takeUntil(new Predicate<PollingResult>() {

                    @Override
                    public boolean test(PollingResult pollingResult) throws Exception {
                        final boolean updatesCompleted = pollingResult.isUpdatesCompleted();
                        Log.d("COMPLETED", "UPDATE COMPLETED? -> " + updatesCompleted);
                        return updatesCompleted;
                    }
                })
                .doOnNext(new Consumer<PollingResult>() {
                    @Override
                    public void accept(PollingResult pollingResult) throws Exception {
                        mSkyScanRepository.loadData(pollingResult);
                    }
                });
    }

    @Nullable
    private static String getSessionKey(Response<SessionResponse> responseResponse) {
        final Uri uri = Uri.parse(responseResponse.headers().get(LOCATION_HEADER_NAME));
        final List<String> pathSegments = uri.getPathSegments();
        if (pathSegments != null && pathSegments.size() > 0) {
            return pathSegments.get(pathSegments.size() - 1);
        } else {
            return null;
        }
    }
}
