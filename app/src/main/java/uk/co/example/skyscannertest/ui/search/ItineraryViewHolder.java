package uk.co.example.skyscannertest.ui.search;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import uk.co.example.skyscannertest.R;
import uk.co.example.skyscannertest.ui.common.util.UI;
import uk.co.example.skyscannertest.ui.view.AgentView;
import uk.co.example.skyscannertest.ui.view.SegmentView;

/**
 * This is the ViewHolder pattern implementation for the Itinerary model
 */

public class ItineraryViewHolder extends RecyclerView.ViewHolder {
    
    private final LinearLayout mSegmentContainer;
    private final AgentView mAgentView;


    public ItineraryViewHolder(View itemView) {
        super(itemView);
        mSegmentContainer = UI.findViewById(itemView, R.id.segmentContainer);
        mAgentView = UI.findViewById(itemView, R.id.agentView);
    }


    public void displayModel(ItineraryViewModel model) {
        for (int i = 0; i < mSegmentContainer.getChildCount(); i++) {
            final SegmentView child = (SegmentView) mSegmentContainer.getChildAt(i);
            child.recycle();
        }
        mSegmentContainer.removeAllViews();
        for (SegmentViewModel segmentViewModel : model.getSegments()) {
            final SegmentView segmentView = SegmentView.obtain(mSegmentContainer.getContext());
            segmentView.setSegmentViewModel(segmentViewModel);
            mSegmentContainer.addView(segmentView);
        }
        mAgentView.setAgentViewModel(model.getAgentViewModel());
    }
}
