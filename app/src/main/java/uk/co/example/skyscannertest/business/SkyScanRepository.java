package uk.co.example.skyscannertest.business;

import android.support.annotation.Nullable;

import uk.co.example.skyscannertest.business.model.Agent;
import uk.co.example.skyscannertest.business.model.Carrier;
import uk.co.example.skyscannertest.business.model.Leg;
import uk.co.example.skyscannertest.business.model.Place;
import uk.co.example.skyscannertest.business.model.PollingResult;
import uk.co.example.skyscannertest.business.model.Segment;
import uk.co.example.skyscannertest.ui.common.mvp.Repository;

/**
 * This is the abstraction for the component who manages data
 */

public interface SkyScanRepository extends Repository {

    /**
     * This is the method we use in order to load new data into the SkyScanRepository
     *
     * @param pollingResult The new available result
     */
    void loadData(PollingResult pollingResult);

    /**
     * Returns the Agent for the given id if any
     *
     * @param agentId The id for the Agent to look up
     * @return The Agent with the given id if any or null if it doesn't exist
     */
    @Nullable
    Agent findAgentById(int agentId);

    /**
     * Returns the Leg for the given id if any
     *
     * @param legId The id of the Leg to retrieve
     * @return The Leg with the given id if any or null if it doesn't exist
     */
    @Nullable
    Leg findLegById(String legId);

    /**
     * Return the Segment for the given id
     *
     * @param segmentId The id of the Segment to retrieve
     * @return The Segment with the given id if any or null if it doesn't exist
     */
    @Nullable
    Segment findSegmentById(int segmentId);

    /**
     * Return the Place for the given id
     *
     * @param placeId The id of the Place to retrieve
     * @return The Place with the given id if any or null if it doesn't exist
     */
    @Nullable
    Place findPlaceById(int placeId);

    /**
     * Return the Carrier for the given id
     *
     * @param carrierId The id of the Carrier to retrieve
     * @return The Carrier with the given id if any or null if it doesn't exist
     */
    @Nullable
    Carrier findCarrierById(int carrierId);
}
