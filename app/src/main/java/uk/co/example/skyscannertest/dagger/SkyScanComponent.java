package uk.co.example.skyscannertest.dagger;

import javax.inject.Singleton;

import dagger.Component;
import uk.co.example.skyscannertest.ui.MainActivity;
import uk.co.example.skyscannertest.ui.search.SearchResultFragment;
import uk.co.example.skyscannertest.ui.main.MainFragment;

/**
 * This is the Dagger component
 */
@Component(modules = {NetworkModule.class,
        PresenterModule.class, RepositoryModule.class,
        ResourceModule.class, UseCaseModule.class})
@Singleton
public interface SkyScanComponent {

    void inject(MainActivity mainActivity);

    void inject(MainFragment mainFragment);

    void inject(SearchResultFragment mainActivityFragment);
}
