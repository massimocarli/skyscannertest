package uk.co.example.skyscannertest.ui.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Utility class for method about time as string
 */

public class TimeUtil {

    private static final int MINUTES_IN_HOUR = 60;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final DateFormat SHORT_DATE_FORMAT = new SimpleDateFormat("d MMM");

    private TimeUtil() {
    }

    /**
     * Converts a duration in minutes in a String for hours and minutes
     *
     * @param duration The duration in minutes
     * @return The String in the format xh ym
     */
    public static String durationToString(final int duration) {
        if (duration < MINUTES_IN_HOUR) {
            return String.format("%dm", duration);
        } else {
            return String.format("%dh %dm", duration / MINUTES_IN_HOUR, duration % MINUTES_IN_HOUR);
        }
    }

    public static Date nextDayOf(Date fromDate, int dayName) {
        final Calendar date = Calendar.getInstance();
        date.setTime(fromDate);
        int weekday = date.get(Calendar.DAY_OF_WEEK);
        if (weekday != dayName) {
            int days = (Calendar.SATURDAY - weekday + 2) % 7;
            date.add(Calendar.DAY_OF_YEAR, days);
        }
        return date.getTime();
    }

    public static Date tomorrow(Date thisDay) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(thisDay);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }


    public static String asString(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static String asShortString(Date date) {
        return SHORT_DATE_FORMAT.format(date);
    }
}
